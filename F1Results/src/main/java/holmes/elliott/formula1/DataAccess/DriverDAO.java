package holmes.elliott.formula1.DataAccess;

import java.util.List;

import holmes.elliott.formula1.Data.DataStore;
import holmes.elliott.formula1.Model.Driver;

public class DriverDAO {

	public boolean addDriver(Driver driver) {
		DataStore.getInstance().getDrivers().add(driver);
		return true;
	}

	public List<Driver> getDrivers() {
		return DataStore.getInstance().getDrivers();
	}
}
