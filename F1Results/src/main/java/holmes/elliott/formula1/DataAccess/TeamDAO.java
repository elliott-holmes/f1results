package holmes.elliott.formula1.DataAccess;

import java.util.List;

import holmes.elliott.formula1.Data.DataStore;
import holmes.elliott.formula1.Model.Team;

public class TeamDAO {

	public boolean addTeam(Team team) {
		DataStore.getInstance().getTeams().add(team);
		return true;
	}

	public List<Team> getTeams() {
		return DataStore.getInstance().getTeams();
	}

}
