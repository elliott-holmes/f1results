package holmes.elliott.formula1.Tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import holmes.elliott.formula1.DataAccess.DriverDAO;
import holmes.elliott.formula1.Model.Driver;

public class DriverDAOTest {

	DriverDAO driverDAO;

	@Before
	public void setUp() throws Exception {
		driverDAO = new DriverDAO();
	}

	@Test
	public void testAddDriver() {
		Driver driver1 = new Driver();
		driver1.setName("Elliott Holmes");
		driver1.setPoints(123);
		Driver driver2 = new Driver();
		driver2.setName("Michael Schumacher");
		driver2.setPoints(121);
		Driver driver3 = new Driver();
		driver3.setName("Damon Hill");
		driver3.setPoints(115);
		assertTrue(driverDAO.addDriver(driver1));
		assertTrue(driverDAO.addDriver(driver2));
		assertTrue(driverDAO.addDriver(driver3));
	}

	@Test
	public void testGetDrivers() {
		int currentSize = driverDAO.getDrivers().size();
		Driver driver4 = new Driver();
		driver4.setName("Rubens Barrichello");
		driver4.setPoints(88);
		driverDAO.addDriver(driver4);
		Driver driver5 = new Driver();
		driver5.setName("Eddie Irvine");
		driver5.setPoints(71);
		driverDAO.addDriver(driver5);

		assertEquals(currentSize + 2, driverDAO.getDrivers().size());
	}

	@After
	public void tearDown() throws Exception {
		driverDAO = null;
	}

}
