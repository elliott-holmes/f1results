package holmes.elliott.formula1.Tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ DriverDAOTest.class, TeamDAOTest.class })
public class AllTests {

}
