package holmes.elliott.formula1.Tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import holmes.elliott.formula1.DataAccess.TeamDAO;
import holmes.elliott.formula1.Model.Team;

public class TeamDAOTest {

	TeamDAO teamDAO;

	@Before
	public void setUp() throws Exception {
		teamDAO = new TeamDAO();
	}

	@Test
	public void testAddTeam() {
		Team team1 = new Team();
		team1.setName("Williams");
		team1.setPoints(189);
		Team team2 = new Team();
		team2.setName("Ferrari");
		team2.setPoints(165);
		Team team3 = new Team();
		team3.setName("Benetton");
		team3.setPoints(140);
		assertTrue(teamDAO.addTeam(team1));
		assertTrue(teamDAO.addTeam(team2));
		assertTrue(teamDAO.addTeam(team3));

	}

	@Test
	public void testGetTeams() {
		int currentSize = teamDAO.getTeams().size();
		Team team1 = new Team();
		team1.setName("Williams");
		team1.setPoints(189);
		Team team2 = new Team();
		team2.setName("Ferrari");
		team2.setPoints(165);
		Team team3 = new Team();
		team3.setName("Benetton");
		team3.setPoints(140);
		Team team4 = new Team();
		team1.setName("Lotus");
		team1.setPoints(101);
		Team team5 = new Team();
		team2.setName("Jordan");
		team2.setPoints(99);
		teamDAO.addTeam(team1);
		teamDAO.addTeam(team2);
		teamDAO.addTeam(team3);
		teamDAO.addTeam(team4);
		teamDAO.addTeam(team5);
		assertEquals(currentSize + 5, teamDAO.getTeams().size());
	}

	@After
	public void tearDown() throws Exception {
		teamDAO = null;
	}

}
