package holmes.elliott.formula1.View;

import java.util.Scanner;

import com.google.gson.Gson;

import holmes.elliott.formula1.Controller.ResultFetcher;
import holmes.elliott.formula1.Model.ResultContainer;

public class Requester {

	public static void main(String[] args) {
		// Check for some parameter for URL
		String urlToParse;
		if (args.length != 1) {
			Scanner in = new Scanner(System.in);
			System.out.println("Please provide a single parameter of the URL to query :-");
			urlToParse = in.nextLine();
			in.close();
		} else {
			urlToParse = args[0];
		}

		// Fetch Results and return them
		ResultFetcher resultFetcher = new ResultFetcher();
		ResultContainer results = resultFetcher.fetchResults(urlToParse);
		Gson gson = new Gson();
		System.out.println(gson.toJson(results));
	}
}
