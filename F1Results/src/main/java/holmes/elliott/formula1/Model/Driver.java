package holmes.elliott.formula1.Model;

import java.io.Serializable;

import holmes.elliott.formula1.Interfaces.ResultHolder;

public class Driver implements ResultHolder, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6799648829091183790L;
	private String name;
	private Integer points;

	/**
	 * @return the name
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	@Override
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the points
	 */
	@Override
	public Integer getPoints() {
		return points;
	}

	/**
	 * @param points
	 *            the points to set
	 */
	@Override
	public void setPoints(Integer points) {
		this.points = points;
	}

}
