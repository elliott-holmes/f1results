package holmes.elliott.formula1.Model;

import java.util.List;

public class ResultContainer {
	private List<Driver> drivers;
	private List<Team> teams;
	private String errorString;

	/**
	 * @return the drivers
	 */
	public List<Driver> getDrivers() {
		return drivers;
	}

	/**
	 * @param drivers
	 *            the drivers to set
	 */
	public void setDrivers(List<Driver> drivers) {
		this.drivers = drivers;
	}

	/**
	 * @return the teams
	 */
	public List<Team> getTeams() {
		return teams;
	}

	/**
	 * @param teams
	 *            the teams to set
	 */
	public void setTeams(List<Team> teams) {
		this.teams = teams;
	}

	/**
	 * @return the errorString
	 */
	public String getErrorString() {
		return errorString;
	}

	/**
	 * @param errorString
	 *            the errorString to set
	 */
	public void setErrorString(String errorString) {
		this.errorString = errorString;
	}

}
