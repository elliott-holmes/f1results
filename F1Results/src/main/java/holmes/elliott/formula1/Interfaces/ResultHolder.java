package holmes.elliott.formula1.Interfaces;

public interface ResultHolder {

	public void setName(String name);

	public String getName();

	public void setPoints(Integer points);

	public Integer getPoints();

}
