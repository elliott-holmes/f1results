/**
 * 
 */
package holmes.elliott.formula1.Controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import holmes.elliott.formula1.DataAccess.DriverDAO;
import holmes.elliott.formula1.DataAccess.TeamDAO;
import holmes.elliott.formula1.Model.Driver;
import holmes.elliott.formula1.Model.ResultContainer;
import holmes.elliott.formula1.Model.Team;

/**
 * this is the main class that does all the work
 *
 */
public class ResultFetcher {

	private DriverDAO driverDAO;
	private TeamDAO teamDAO;

	public ResultContainer fetchResults(String urlToFetch) {
		ResultContainer returnContainer = new ResultContainer();
		PrintWriter outFile = null;

		try {
			outFile = new PrintWriter(new File("apperr.log"));
		} catch (FileNotFoundException fnfe) {
			// Not going to highlight this error. Errors will just appear
			// on the console.
			outFile = null;
		}

		try {
			URL url = new URL(urlToFetch);
			Document resultDoc = Jsoup.parse(url, 30000);
			Element driversTable = resultDoc.select("table.msr_season_driver_results").get(0);
			Element teamTable = resultDoc.select("table.msr_season_team_results").get(0);
			driversTable.select("tbody").select("tr").stream().forEach(this::handleDriverRow);
			teamTable.select("tbody").select("tr").stream().forEach(this::handleTeamRow);
		} catch (MalformedURLException mue) {
			returnContainer.setErrorString("There is a problem with the format of your url. Please check and try again");
			handleErr(mue, outFile);
		} catch (IOException ioe) {
			returnContainer.setErrorString("There is a problem connecting to the url you have provided. Please check and try again");
			handleErr(ioe, outFile);
		} catch (NumberFormatException nfe) {
			returnContainer.setErrorString("There is a problem with the format of the table. Please check and try again");
			handleErr(nfe, outFile);
		}

		returnContainer.setDrivers(getDriverDAO().getDrivers());
		returnContainer.setTeams(getTeamDAO().getTeams());

		return returnContainer;
	}

	private void handleDriverRow(Element tableRow) throws NumberFormatException {
		if (getDriverDAO().getDrivers().size() == 10) {
			return;
		}
		Driver driver = new Driver();
		driver.setName(tableRow.select("td.msr_driver").get(0).select("a").get(0).text());
		try {
			driver.setPoints(Integer.parseInt(tableRow.select("td.msr_total").get(0).text()));
		} catch (NumberFormatException nfe) {
			System.err.println("Error retrieving points value for : " + driver.getName());
			driver.setPoints(-1);
			throw nfe;
		}
		getDriverDAO().addDriver(driver);
	}

	private void handleTeamRow(Element tableRow) {
		// slightly different as may need to handle row spans.
		if (getTeamDAO().getTeams().size() == 5) {
			return;
		}
		// Check if there is a team name cell
		Elements teamNameCells = tableRow.select("td.msr_team");
		if (!teamNameCells.isEmpty()) {
			Team team = new Team();
			team.setName(teamNameCells.get(0).select("a").get(0).text());
			try {
				team.setPoints(Integer.parseInt(tableRow.select("td.msr_total").get(0).text()));
			} catch (NumberFormatException nfe) {
				System.err.println("Error retrieving points value for : " + team.getName());
				team.setPoints(-1);
				throw nfe;
			}
			getTeamDAO().addTeam(team);
		}

	}

	/**
	 * @return the driverDAO
	 */
	private DriverDAO getDriverDAO() {
		if (driverDAO == null) {
			driverDAO = new DriverDAO();
		}
		return driverDAO;
	}

	/**
	 * @return the teamDAO
	 */
	private TeamDAO getTeamDAO() {
		if (teamDAO == null) {
			teamDAO = new TeamDAO();
		}
		return teamDAO;
	}

	private void handleErr(Exception e, PrintWriter pw) {
		if (pw != null) {
			e.printStackTrace(pw);
		} else {
			e.printStackTrace(System.err);
		}
	}
}
