package holmes.elliott.formula1.Data;

import java.util.LinkedList;
import java.util.List;

import holmes.elliott.formula1.Model.Driver;
import holmes.elliott.formula1.Model.Team;

public class DataStore {

	private static final DataStore INSTANCE = new DataStore();
	private LinkedList<Driver> drivers;
	private LinkedList<Team> teams;

	private DataStore() {
		// Private constructor so cannot be instantiated externally
	}

	/**
	 * @return the drivers
	 */
	public List<Driver> getDrivers() {
		if (drivers == null) {
			drivers = new LinkedList<Driver>();
		}
		return drivers;
	}

	/**
	 * @return the teams
	 */
	public List<Team> getTeams() {
		if (teams == null) {
			teams = new LinkedList<Team>();
		}
		return teams;
	}

	/**
	 * @return the instance
	 */
	public static DataStore getInstance() {
		return INSTANCE;
	}

}
