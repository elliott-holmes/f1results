# README #



### What is this repository for? ###

This repository retrieves a set of Formula 1 results from a specific site  and outputs them in JSON format. The URL is http://www.f1-fansite.com/f1-results/2015-f1-results-standings/

### Getting Started ###
Download this code into your favourite IDE (I prefer Eclipse, so I know it will work in there, but any that supports git should be fine) or you could use command line, but I can't help on that one.

When downloaded please build the executable jar using gradle fatJar (fat because it contains everything). This will build you a standalone JAR file to execute.

Then run this JAR File F1Results-all-1.0.jar, found in the build/libs directory. run from a command line, either passing the url parameter in, or not...see what happens.

Any problems with this please let me know and I will try and resolve the issues as quickly as i can.